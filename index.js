"use strict";
// console.log("Siesta Time");

// Array Methods

    // JavaScript has built-in function for arrays. This allows is to manipulate and access array elements.

    // [Section] Mutator Methods
        // Mutator methods are functions that mutate or change and array after they're created.
        // These methods manipulate the original array performing various tasks as adding and removing elements.
        
            let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];  
            console.log("Current array fruits: ");
            console.log(fruits);

        /* 
            push()
            - adds an element in the end of an array and returns the array's length
        */ 
            let fruitsLen = fruits.push("Mango", "Avocado", "Guava");
            console.log("Mutated array after the push method: ");
            console.log(fruits);
            console.log(fruitsLen);

        /* 
            pop()
            - removes the last element in an array and returns the removed element
        */
            let removedFruit =  fruits.pop();
            console.log("Mutated array after the pop method: ");
            console.log(fruits);
            console.log(removedFruit);

        /* 
            unshift()
            - add one or more elements at the beginning of an array and returns the array's length 
        */
            fruitsLen = fruits.unshift("Lime", "Banana");
            console.log("Mutated array after the unshift method: ");
            console.log(fruits);
            console.log(fruitsLen);

        /* 
            shift()
            - removes an element at the beginning of an array and it returns the removed elements.
        */
            removedFruit =  fruits.shift();
            console.log("Mutated array after the shift method: ");
            console.log(fruits);
            console.log(removedFruit);

        /* 
            splice() 
            - simultaneously removes elements from a specified index number and adds elements. Returns the removed elements.
        */
            fruits.splice(3, 0, "Cherry" ,"Grapes");
            console.log("Mutated array after the splice method: ");
            console.log(fruits);

            // remove only using splice()
            console.log(fruits.splice(3, 2));
            console.log("Mutated array after the splice method: ");
            console.log(fruits);

        /* 
            sort() 
            - rearranges the array elements in alphanumeric order 
        */
            fruits.sort();
            console.log("Mutated array after the sort method: ");
            console.log(fruits);

        /* 
            reverse() 
            - reverses the order of array elements 
        */
            fruits.reverse();
            console.log("Mutated array after the reverse method: ");
            console.log(fruits);

    // [Section] Non-Mutator Methods
        // are functions that do not modify or change an array after they're created.
        // these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing output.

        let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

        /* 
            indexOf() 
            - returns the index number of the first matching element found in an array. 
            - if no match, the result will be -1. 
            - the search process will be done from first element proceeding to the last element. 
        */

            console.log(countries.indexOf("PH"));       // Without starting index
            console.log(countries.indexOf("BR"));       // No match
            console.log(countries.indexOf("PH", 2));    // With starting index

        /* 
            lastIndexOf()
            - it returns the index number of the last matching element found in an array.
            - if no match, the result will be -1. 
            - the search process will be done from last element proceesing the first element.
        */

            console.log(countries.lastIndexOf("PH"));       // Without starting index
            console.log(countries.lastIndexOf("BR"));       // No match
            console.log(countries.lastIndexOf("PH", 4));    // With starting index

        /* 
            slice()
            - portion/slices from an array and return a new array
        */

            // slicing off elemtns from a specified index to the last element
            console.log(countries);
            let slicedArrayA = countries.slice(2);
            console.log(slicedArrayA);

            // slicing off elements from a specified index to another index.
            console.log(countries);
            let slicedArrayB = countries.slice(1, 5);
            console.log(slicedArrayB);

            // slicing off elements from the last element of an array
            console.log(countries);
            let slicedArrayC = countries.slice(-5, -3);
            console.log(slicedArrayC);

        /* 
            toString()
            - returns an array as string separated by commas
        */
            console.log(countries.toString());

        /* 
            concat()
            - combines arrays and returns the combined result
        */

            let taskArrayA = ["drink HTML", "eat JS"];
            let taskArrayB = ["inhale CSS", "breathe SASS"];
            let taskArrayC = ["git gud"];

            let task = taskArrayA.concat(taskArrayB, taskArrayC);
            console.log(task);

            // Combine array with elements
            let combineTasks = task.concat("smell express", "throw react");
            console.log(combineTasks);

        /* 
            join()
            - return an array as a string separated by specified separator string
        */

            let users = ["John", "Jane", "Joe", "Robert"];
            console.log(users.join());
            console.log(users.join(" "));

    // [Section] Iteration Methods

        /* 
            Iteration methods 
            - are loop designed to perform repetitive tasks on arrays.
            - loops over all elements in arrays.
        */

        /* 
            forEach()
            - similar to a for loop that iterates on each array element
            - for each element in the array, the function in the forEach method will be run.
        */
            console.log(combineTasks);
            combineTasks.forEach(function(task) {
                console.log(task);
            })

            let filteredTasks = [];

            combineTasks.forEach(function(task) {
                if (task.length>10) {
                    filteredTasks.push(task);
                }
            })

            console.log(filteredTasks);

        /* 
            map()
            - it iterates on each element and returns new array with a different values depending on the result of the function's operation.
        */
            let numbers = [1, 2, 3, 4, 5];

            let numberMap = numbers.map(function(number) {
                return number*number;
            })

            console.log("Original Array: ");
            console.log(numbers);
            console.log("Result of map method");
            console.log(numberMap);

        /* 
            every()
            - checks if all the elements in an array meet the given condition
            - useful in validating data stored in arrays especially when dealing with large amounts of data
            - returns a true value if all elements meet the condition and false if otherwise
        */
            console.log(numbers);
            let allValid = numbers.every(function(num){
                return (num<=5);
            })

            console.log(allValid);

        /* 
            some()
            - checks if at least one of the element in an array meets the given condition
            - returns a true value if at least one element meet the condition and false if none
        */
            console.log(numbers);
            let someValid = numbers.some(function(num){
                return (num>5);
            })

            console.log(someValid);

        /* 
            filter()
            - return new array that contains the elements which meets the given condition
            - will return an empty array if no elements were found.
        */
            console.log(numbers);
            let filterValid = numbers.filter(num => { 
                return (num<4);
            })

            console.log(filterValid);

        /* 
            includes()
            - checks if the arguments passed can be found in the array
            - returns boolean which can be save in a variable
        */
            const products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
            console.log(products);

            const productsFound1 = products.includes("Mouse");
            console.log(productsFound1);

            const productsFound2 = products.includes("Headset");
            console.log(productsFound2);

        /* 
            reduce()
            - it will evaluate elements from left to right and return or reduces the array into a single value
        */
            console.log(numbers);
            let total = numbers.reduce(function(x,y){
                console.log("This is the value of x: " + x);
                console.log("This is the value of y: " + y);
                return x+y;
            })

            console.log(total);